<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta12(){
        $datosTrabajadores= Trabajadores::find()->all();
        return $this->render('consulta12',['trabajadores'=>$datosTrabajadores]);
    }
    
    public function actionConsulta14(){
        
        /*con  ActiveRecord */
        $datosDelegaciones= Delegacion::find()->all();
        
        /*con create commmand*/
        $datosDelegaciones1= \Yii::$app->db
                ->createCommand("SELECT * FROM delegacion")
                ->queryAll();
        
        /*con Query Builder*/
         $query=new \yii\db\Query();
         $datosDelegaciones2=$query->select('*')
                                ->from('delegacion')
                                ->all();
         
         /*llamar a la vista*/
        return $this->render('consulta14',['datos1'=>$datosDelegaciones,
            'datos2'=>$datosDelegaciones1,
            'datos3'=>$datosDelegaciones2]);
        
    }
    
    public function actionConsulta17(){
        //a.listar las delegaciones cuya población sea Santander:
        //con ActiveRecord
        $consulta1ar= Delegacion::find()
                ->where(['poblacion'=>'Santander'])
                ->all();
        //lo mismo con createCommand
        $consulta1cc=\Yii::$app->db->createCommand("SELECT * FROM delegacion WHERE poblacion LIKE 'Santander'")
                    ->queryAll();
   
        //b.listar los trabajadores de la delegacion 1:
        //con ActiveRecord
        $consulta2ar= Trabajadores::find()
                ->where(['delegacion'=>1])
                ->all();
        //lo mismo con createCommand
        $consulta2cc=\Yii::$app->db->createCommand("SELECT * FROM trabajadores WHERE delegacion=1")
                        ->queryAll();
                
        //c.listar los trabajadores ordenados por nombre:
        //con ActiveRecord
        $consulta3ar= Trabajadores::find()
                ->orderBy('nombre')
                ->all();
        //lo mismo con createCommand
        $consulta3cc=\Yii::$app->db->createCommand("SELECT * FROM trabajadores ORDER BY nombre")
                ->queryAll();
                
        //d.listar los trabajadores que no conozco la fecha de nacimiento:
        //con ActiveRecord
        $consulta4ar= Trabajadores::find()
                ->where(['fechaNacimiento'=>null])
                ->all();
        //lo mismo con createCommand
        $consulta4cc=\Yii::$app->db->createCommand("SELECT * FROM trabajadores WHERE fechaNacimiento IS NULL")
                    ->queryAll();
        
        return $this->render('consulta17',
                ['consulta1ar'=>$consulta1ar,
                'consulta1cc'=>$consulta1cc,
                'consulta2ar'=>$consulta2ar,
                'consulta2cc'=>$consulta2cc,
                'consulta3ar'=>$consulta3ar,
                'consulta3cc'=>$consulta3cc,
                'consulta4ar'=>$consulta4ar,
                'consulta4cc'=>$consulta4cc]);
    }
    
    public function actionConsulta20(){ 
        /*Lista las delegaciones que tengan una poblacion distinta a Santander y de las cuales sepamos la direccion*/
        //con ActiveRecord
        $query1ar= Delegacion::find()
                ->where(['!=', 'poblacion', 'Santander'])
                ->andWhere(['IS NOT','direccion',NULL])
                ->all();
        
        //Con CreateCommand
        $query1cc= \Yii::$app->db->createCommand('SELECT * FROM delegacion WHERE poblacion <>"Santander" AND direccion IS NOT NULL')
                ->queryAll();
        
       
        
        /*Listar los trabajadores de las delegaciones de Santander*/
        $query2ar=Trabajadores::find()
                ->joinWith('delegacion0')
                ->where(['poblacion'=>'Santander'])
                ->all();
                
        $query2cc=\yii::$app->db->
                createCommand('SELECT  trabajadores.* FROM trabajadores JOIN delegacion ON trabajadores.delegacion=delegacion.id  WHERE poblacion="Santander";')
                ->queryAll();
        
         
         /*Listar los trabajadores y las delegaciones de los trabajadores que tengan foto*/
         
         $query3ar= Trabajadores::find()
                 ->With('delegacion0')
                 ->where('foto is NOT NULL')
                 ->all();
         
         $query3cc=\yii::$app->db->
         createCommand('SELECT * FROM trabajadores JOIN delegacion ON trabajadores.delegacion=delegacion.id WHERE foto IS NOT NULL;')
                 ->queryAll();
         
         
          
        /*Sacar las delegaciones que no tienen trabajadores*/
          
          $query4ar= Delegacion::find()
                  ->leftJoin('trabajadores', 'delegacion.id=trabajadores.delegacion')
                  ->where('delegacion IS NULL')
                  ->all();
          
          $query4cc=\yii::$app->db->
          createCommand('SELECT delegacion.* FROM delegacion LEFT JOIN trabajadores ON delegacion.id=trabajadores.delegacion WHERE trabajadores.delegacion IS NULL;')
                  ->queryAll();
          
           return $this->render('consulta20',['query1ar'=>$query1ar,
                                            'query1cc'=>$query1cc,
                                            'query2ar'=>$query2ar,
                                            'query2cc'=>$query2cc,
                                            'query3ar'=>$query3ar,
                                            'query3cc'=>$query3cc,
                                            'query4ar'=>$query4ar,
                                            'query4cc'=>$query4cc]); 
                  
  
    }
    
    public function actionConsulta22(){
        $datos=[];
        $datos[]= Trabajadores::getNombrecompleto();
        $datos[]= Trabajadores::getNombrecompleto2();
        return $this->render('consulta22',['datos'=>$datos]);
    }
 
}
