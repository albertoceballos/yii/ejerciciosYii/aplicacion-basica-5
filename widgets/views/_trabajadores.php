<?php
    use yii\helpers\Html;
?>    
<div class="row row-flex row-flex-wrap">  
<?php
    foreach($modelos as $trabajador){
?>
<div class="col-sm-6 col-md-3 flex-grow">
    <div class="thumbnail">
      <?=   Html::img("@web/imgs/$trabajador->foto")?>
      <div class="caption">
        <h3><?= $trabajador->id ?></h3>
        <ul>
            <li>Nombre: <?= $trabajador->nombre ?></li>
            <li>Apellidos:<?= $trabajador->apellidos ?> </li>
        </ul>
      </div>
    </div>
  </div>
<?php
  }
?>
</div>


